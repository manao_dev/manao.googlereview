<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'manao.googlereview'; // $module_id prefer than another name

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");// for use original language constants
Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight($module_id)<"S")
{
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

//tabs and fields description

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('MANAO_GOOGLEREVIEW_TAB_SETTINGS'),
        "TITLE" => Loc::getMessage("MANAO_GOOGLEREVIEW_EDIT1_DESCRIPTION"),
        'OPTIONS' => array(
            array('merchant_id', Loc::getMessage('MANAO_GOOGLEREVIEW_MERCHANT_ID'),
                '',
                array('text', 10)),
            array('country_id', Loc::getMessage('MANAO_GOOGLEREVIEW_COUNTRY_ID'),
                '',
                array('multiselectbox',
                    array(
                        'BY'=>Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_BY'),
                        'RU'=>Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_RU'),
                        'UA'=>Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_UA'),
                        'KZ'=>Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_KZ')                        
                    )
                )
            ),
            array('opt_in_style', Loc::getMessage('MANAO_GOOGLEREVIEW_OPT_IN_STYLE'),
                '',
	            array('multiselectbox',
	            	array(
		            	'CENTER_DIALOG'=>Loc::getMessage('MANAO_GOOGLEREVIEW_CENTER_DIALOG'),
		            	'BOTTOM_RIGHT_DIALOG'=>Loc::getMessage('MANAO_GOOGLEREVIEW_BOTTOM_RIGHT_DIALOG'),
		            	'BOTTOM_LEFT_DIALOG'=>Loc::getMessage('MANAO_GOOGLEREVIEW_BOTTOM_LEFT_DIALOG'),
		            	'TOP_RIGHT_DIALOG'=>Loc::getMessage('MANAO_GOOGLEREVIEW_TOP_RIGHT_DIALOG'),
		            	'TOP_LEFT_DIALOG'=>Loc::getMessage('MANAO_GOOGLEREVIEW_TOP_LEFT_DIALOG'), 
		            	'BOTTOM_TRAY'=>Loc::getMessage('MANAO_GOOGLEREVIEW_BOTTOM_TRAY')
		            )
	            )
	        ),
            array('show_again', Loc::getMessage('MANAO_GOOGLEREVIEW_SHOW_AGAIN'),
                '',
                array('checkbox')),
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),
);
//Save data

if ($request->isPost() && $request['Update'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;


            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
}

//Show

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='manao_googlereview_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");

    $tabControl->Buttons(); ?>
    <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>">
   <!--  <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>"> button don't work even on the forms module. -->
    <?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>