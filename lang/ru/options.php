<?
$MESS["MANAO_GOOGLEREVIEW_TAB_SETTINGS"] = "Параметры";
$MESS["MANAO_GOOGLEREVIEW_OPT_IN_STYLE"] = "Расположение модуля";
$MESS["MANAO_GOOGLEREVIEW_CENTER_DIALOG"] = "В центре экрана";
$MESS["MANAO_GOOGLEREVIEW_BOTTOM_RIGHT_DIALOG"] = "В правом нижнем углу";
$MESS["MANAO_GOOGLEREVIEW_BOTTOM_LEFT_DIALOG"] = "В левом нижнем углу";
$MESS["MANAO_GOOGLEREVIEW_TOP_RIGHT_DIALOG"] = "В правом верхнем углу";
$MESS["MANAO_GOOGLEREVIEW_TOP_LEFT_DIALOG"] = "В левом верхнем углу";
$MESS["MANAO_GOOGLEREVIEW_BOTTOM_TRAY"] = "В нижней части экрана";
$MESS["MANAO_GOOGLEREVIEW_EDIT1_DESCRIPTION"] = "Настройки параметров модуля";
$MESS["MANAO_GOOGLEREVIEW_DENIED"] = "Доступ запрещен";
$MESS["MANAO_GOOGLEREVIEW_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["MANAO_GOOGLEREVIEW_WRITE_SETTINGS"] = "Изменение настроек";
$MESS["MANAO_GOOGLEREVIEW_FULL"] = "Полный доступ";
?>