<?
$MESS["MANAO_GOOGLEREVIEW_MODULE_NAME"] = "Гугл опрос";
$MESS["MANAO_GOOGLEREVIEW_MODULE_DESCRIPTION"] = "Модуль подключает опрос от компании Google при создании заказа";
$MESS["MANAO_GOOGLEREVIEW_PARTNER_NAME"] = "Manao";
$MESS["MANAO_GOOGLEREVIEW_PARTNER_URI"] = "https://manao.by";
$MESS["MANAO_GOOGLEREVIEW_MERCHANT_ID"] = "Ваш идентификатор Merchant Center. Его можно посмотреть в <a href='https://www.google.ru/retail/solutions/merchant-center' target='_blank'>Google Merchant Center</a>";
$MESS["MANAO_GOOGLEREVIEW_INSTALL_TITLE"] = "Установка модуля Manao:Googlereview";
$MESS["MANAO_GOOGLEREVIEW_COUNTRY_ID"] = "Страна";
$MESS["MANAO_GOOGLEREVIEW_CONUTRY_BY"] = "Беларусь";
$MESS["MANAO_GOOGLEREVIEW_CONUTRY_RU"] = "Россия";
$MESS["MANAO_GOOGLEREVIEW_CONUTRY_UA"] = "Украина";
$MESS["MANAO_GOOGLEREVIEW_CONUTRY_KZ"] = "Казахстан";
$MESS["MANAO_GOOGLEREVIEW_INSTALL_ERROR"] = "Убедитесь, что версия приложения не ниже 14.0.0 и установлен модуль Интернет магазин";
$MESS["MANAO_GOOGLEREVIEW_HTTPS_ERROR"] = "Для корректной работы модуля необходимо подключить https протокол передачи данных";
$MESS["MANAO_GOOGLEREVIEW_ALERT"] = "ВНИМАНИЕ!!!";
$MESS["MANAO_GOOGLEREVIEW_SHOW_AGAIN"] = "Разрешить показывать js код повторно";
?>