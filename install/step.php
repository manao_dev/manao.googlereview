<?
if (!check_bitrix_sessid())
	return;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
?>
<form action="<?echo $APPLICATION->GetCurPage()?>" name="form">
<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?echo LANGUAGE_ID?>">	
	<input type="hidden" name="step" value="1">
	<input type="hidden" name="install" value="Y">	
	<input type="hidden" name="id" value="manao.googlereview">
	<p><b><?=Loc::getMessage('MANAO_GOOGLEREVIEW_MERCHANT_ID')?></b><br><br>
   		<input type="text" name="merchant_id" size="20" required>
  	</p>
  	<p><b><?=Loc::getMessage('MANAO_GOOGLEREVIEW_COUNTRY_ID')?></b><br><br>
	   <input type="radio" name="country_id" value="BY" checked required> <?=Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_BY')?><br>
	   <input type="radio" name="country_id" value="RU" required> <?=Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_RU')?><br>
	   <input type="radio" name="country_id" value="UA" required> <?=Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_UA')?><br>
	   <input type="radio" name="country_id" value="KZ" required> <?=Loc::getMessage('MANAO_GOOGLEREVIEW_CONUTRY_KZ')?><br>
  	</p>
  	<p><br><br>
  		<input type="checkbox" name="show_again" value="Y" checked> <?=Loc::getMessage('MANAO_GOOGLEREVIEW_SHOW_AGAIN')?>
  	</p>
	<input type="submit" name="" value="Далее">
</form>	
