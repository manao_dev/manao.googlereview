<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use \Bitrix\Main\Application;
use Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

final class manao_googlereview extends CModule {
	
	function __construct() {
		$arModuleVersion = array();
		include(__DIR__."/version.php");
		
		$this->MODULE_ID = "manao.googlereview";
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];		
		$this->MODULE_NAME = Loc::getMessage("MANAO_GOOGLEREVIEW_MODULE_NAME");	
		$this->MODULE_DESCRIPTION = Loc::getMessage("MANAO_GOOGLEREVIEW_MODULE_DESCRIPTION");	
		$this->PARTNER_NAME = Loc::getMessage("MANAO_GOOGLEREVIEW_PARTNER_NAME"); 
		$this->PARTNER_URI = Loc::getMessage("MANAO_GOOGLEREVIEW_PARTNER_URI");		
	}
	
	function DoInstall()
	{
		global $APPLICATION;
		$request = Application::getInstance()->getContext()->getRequest();
		if($this->isVersionD7() && Loader::includeModule("sale"))
		{
			if(!$request->isHttps()){
				$APPLICATION->ThrowException(Loc::getMessage("MANAO_GOOGLEREVIEW_HTTPS_ERROR"), "https_error");
			}
			if($request["step"] < 1)
	        {
	            $APPLICATION->IncludeAdminFile(Loc::getMessage("MANAO_GOOGLEREVIEW_INSTALL_TITLE"), $this->GetPath()."/install/step.php");           			
	        } else if($request["step"] == 1) {
	        	$this->InstallEvents();			
				\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
	        	Option::set($this->MODULE_ID, 'country_id', $request["country_id"]);
				Option::set($this->MODULE_ID, 'merchant_id', $request["merchant_id"]);
				Option::set($this->MODULE_ID, 'show_again', $request["show_again"]);
				Option::set('manao.googlereview', 'order_id', "0"); // For the first install
	        	$APPLICATION->IncludeAdminFile(Loc::getMessage("MANAO_GOOGLEREVIEW_INSTALL_TITLE"), $this->GetPath()."/install/step1.php");
	        }			
		}
		else
		{
			$APPLICATION->ThrowException(Loc::getMessage("MANAO_GOOGLEREVIEW_INSTALL_ERROR"));
			$APPLICATION->IncludeAdminFile(Loc::getMessage("MANAO_GOOGLEREVIEW_INSTALL_TITLE"), $this->GetPath()."/install/step1.php");
		}			
	}
	
	function DoUninstall()
	{
		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
		$this->UnInstallEvents();		
		Option::delete($this->MODULE_ID);
	}

	function InstallEvents()
	{
    	\Bitrix\Main\EventManager::getInstance()->registerEventHandler('main', 'OnEndBufferContent', $this->MODULE_ID, '\Manao\Googlereview\Event', 'googlereviewHandler', 1); //
    	return true;    	
	}

	function UnInstallEvents()
	{
    	\Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnEndBufferContent', $this->MODULE_ID, '\Manao\Googlereview\Event', 'googlereviewHandler');    	  	
    	
	}

	//Version BMS must be more or equal 14.00.00 for support D7
	protected function isVersionD7() 
	{
		return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
	}

	//Define location of the module
    protected function GetPath($notDocumentRoot=false)
    {
        if($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(),'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    // if not define, will work default function from /bitrix/modules/main/admin/group_rights.php
    function GetModuleRightList()
    {
        return array(
            "reference_id" => array("D","K","S","W"),
            "reference" => array(
                "[D] ".Loc::getMessage("MANAO_GOOGLEREVIEW_DENIED"),
                "[K] ".Loc::getMessage("MANAO_GOOGLEREVIEW_READ_COMPONENT"),
                "[S] ".Loc::getMessage("MANAO_GOOGLEREVIEW_WRITE_SETTINGS"),
                "[W] ".Loc::getMessage("MANAO_GOOGLEREVIEW_FULL"))
        );
    }
}
?>