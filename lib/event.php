<?php
namespace Manao\Googlereview;
use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;
use Bitrix\Main\Diag\Debug;
class event
{	
    public function googlereviewHandler(&$content) //
    {  
    	$request = \BITRIX\Main\Context::getCurrent()->getRequest();
    	$id = $request["ORDER_ID"]; 
    	$show_again = Option::get('manao.googlereview', 'show_again');   	
    	if($request->isHttps() && isset($id) && !empty($id) && ($show_again == "Y" || $id > Option::get('manao.googlereview', 'order_id')) ) //  set !$request->isHttps() for testing on http requests
    	{ 
    		Option::set('manao.googlereview', 'order_id', $id);
    		$subQuery = \Bitrix\Sale\Internals\OrderPropsTable::query()  //Bitrix\Main\ORM\Query\Query - объект подзапроса
			->setSelect(['ID'])
			->where('ACTIVE', 'Y')
			->where('IS_EMAIL', 'Y');	
						     
			$query = \Bitrix\Sale\Internals\OrderPropsValueTable::query()	//Bitrix\Main\ORM\Query\Query			    
		    ->setSelect([
		        'VALUE',		        		        
		    ])
		    ->whereIn('ORDER_PROPS_ID', $subQuery)  	    
		    ->where('ORDER_ID', $id);

		    $query_res = $query->getQuery();		    
		    $query_fetch = $query->exec()->FetchAll();		   		
			$email = $query_fetch[0]["VALUE"];				 			
	    	$merchant_id = Option::get('manao.googlereview', 'merchant_id');
		    $opt_in_style = Option::get('manao.googlereview', 'opt_in_style', 'CENTER_DIALOG'); 
		    $country_code = Option::get('manao.googlereview', 'country_id');		    		    
		    $lang = \Bitrix\Main\Context::getCurrent()->getLanguage();	 
		    $date = date("Y-m-d"); 
		    if($email && $merchant_id && $country_code && $lang && $date) //
		    {
		    	$str = '<!-- НАЧАЛО кода модуля опроса -->
				<script src="https://apis.google.com/js/platform.js?onload=renderOptIn"
				async defer>
				</script>'.
				'<script>
				window.renderOptIn = function() { 
				window.gapi.load(\'surveyoptin\', function() {
				window.gapi.surveyoptin.render(
					{						          
				    "merchant_id": "'.$merchant_id.'", 
				    "order_id": "'.$id.'",  
				    "email": "'.$email.'",
				    "delivery_country": "'.$country_code.'", 
				    "estimated_delivery_date": "'.$date.'",						          
				    "opt_in_style": "'.$opt_in_style.'" 
					}); 
				});
				}
				</script>
				<!-- КОНЕЦ кода модуля опроса -->'.
				'<!-- НАЧАЛО кода языка опроса -->'.
				'<script>
				window.___gcfg = {
				lang: "'.$lang.'" 
				};
				</script>
				<!-- КОНЕЦ кода языка опроса -->';
				$str .= "</body>";

				$content = str_replace("</body>", $str, $content);															    				    	
		    }  				
    	}    	       
    }    
}